<section className="diaspra-bottom-elementor-section elementor-inner-section elementor-element elementor-element-1fd0176 elementor-section-boxed elementor-section-height-default elementor-section-height-default wpr-particle-no wpr-jarallax-no wpr-parallax-no wpr-sticky-section-no" data-id="1fd0176" data-element_type="section">
					
<div className="diaspra-bottom-elementor-section-container elementor-column-gap-default">
      <div className="diaspra-bottom-elementor-section-column elementor-col-50 elementor-inner-column elementor-element elementor-element-57ed534 animated slideInLeft" data-id="57ed534" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;slideInLeft&quot;}">
<div className="diaspra-bottom-elementor-section-widget-wrap elementor-element-populated">
                  <div className="diaspra-bottom-elementor-sectionelementor-element elementor-element-09affb3 elementor-widget elementor-widget-elementskit-heading" data-id="09affb3" data-element_type="widget" data-widget_type="elementskit-heading.default">
  <div className="diaspra-bottom-elementor-section-widget-container">
<div class="ekit-wid-con"><div class="ekit-heading elementskit-section-title-wraper center   ekit_heading_tablet-   ekit_heading_mobile-"><h2 class="ekit-heading--title elementskit-section-title ">Diaspora <span>Accounts</span></h2></div></div>		</div>
  </div>
  <div className="diaspra-bottom--element elementor-element-267b612 elementor-widget elementor-widget-text-editor" data-id="267b612" data-element_type="widget" data-widget_type="text-editor.default">
  <div className="diaspra-bottom--widget-container">
              <p>Diaspora Banking Account allows Diaspora who resides and works outside the country to maintain and perform domestic and international transfers through their Coopbank accounts.</p>
<p>» Diaspora Current Account<br />» Diaspora Fixed-Time Deposit Account<br />» Diaspora Non-Repatriable Account<br />» Ethiopian Citizen Or Origin Living In Foreign Land (ECOLFL) Savings Account<br /><br /></p>						</div>
  </div>
      </div>
</div>
  <div className="diaspra-bottom--column elementor-col-50 elementor-inner-column elementor-element elementor-element-cf6cde5 animated slideInRight" data-id="cf6cde5" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;slideInRight&quot;}">
<div className="diaspra-bottom--widget-wrap elementor-element-populated">
                  <div className="diaspra-bottom--element elementor-element-410ccbc elementor-widget elementor-widget-elementskit-heading" data-id="410ccbc" data-element_type="widget" data-widget_type="elementskit-heading.default">
  <div className="diaspra-bottom--widget-container">
<div class="ekit-wid-con"><div class="ekit-heading elementskit-section-title-wraper center   ekit_heading_tablet-   ekit_heading_mobile-"><h2 class="ekit-heading--title elementskit-section-title ">Diaspora <span>Loan</span></h2></div></div>		</div>
  </div>
  <div className="diaspra-bottom--element elementor-element-ae1ac64 elementor-widget elementor-widget-text-editor" data-id="ae1ac64" data-element_type="widget" data-widget_type="text-editor.default">
  <div className="diaspra-bottom--widget-container">
              <p>The bank provides business and investment loan along with expertise free consultancy services on different opportunities.<br />» Diaspora consumer loans<br />» Mortgage loan<br />» Car/Vehicle loan<br />» Personal loan<br />» Diaspora Business loans<br />» Investment financing<br />» Working capital loans</p>						</div>
  </div>
      </div>
</div>
              </div>
</section>