/** @format */

import React from "react";
import { Box, Typography } from "@mui/material";

const StatBox = ({ title, subtitle, icon, progress, increase }) => {
  return (
    <Box width='100%' m='0 30px'>
      <Box display='flex' justifyContent='space-between' textAlign="center">
        <Box>
          {icon}
          <Typography variant='h4' fontWeight='bold'>
            {increase}
          </Typography>
          <Typography variant='h5'>{title}</Typography>
        </Box>
      </Box>
      {/* <Box display='flex' justifyContent='space-between' mt='2px'>
        <Typography variant='h5' fontStyle='italic'>
          {increase}
        </Typography>
      </Box> */}
    </Box>
  );
};

export default StatBox;
