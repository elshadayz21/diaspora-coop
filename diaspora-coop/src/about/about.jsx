/** @format */

import { react, useState, useEffect } from "react";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import { Grid, Typography } from "@mui/material";
import statbox from "./statbox";
import PeopleSharpIcon from "@mui/icons-material/PeopleSharp";
import StatBox from "./statbox";
import Slide from "@mui/material/Slide";
import Fade from "@mui/material/Fade";

import { Grid4x4 } from "@mui/icons-material";
// import GroupsIcon from "@mui/icons-material/Groups";
//import { IconButton, MenuIcon } from "@material-ui/core";
const About = () => {
  const fadeInUpEasing = {
    opacity: [0, 1],
    transform: ["translate3d(0,100%,0)", "none"],
  };

  const [isSlideVisible, setIsSlideVisible] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setIsSlideVisible(true);
    }, 500);
  }, [isSlideVisible]);
  const getContainer = () => document.getElementById("slider-component");

  return (
    <Container sx={{ width: "100%" }}>
      <div
        style={{
          display: "flex",
          width: "100%",
        }}>
        <div
          className='container11'
          style={{
            marginLeft: "auto",
            marginRight: "auto ",
            paddingLeft: "20px",
            paddingRight: "20px",
          }}>
          <Box sx={{}}>
            {" "}
            <Grid
             container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}
              >
              <Grid
                item
                lg={6}
                md={6}
                sx={{
                  alignContent: "center",
                  alignItems: "center",
                  // width: "445px",
                  display: "flex",
                  flexWrap: "wrap",
                  position: "relative",
                }}>
                <Box m={"0px 80px 0px 0px"}>
                  <Box>
                    <h6
                      style={{
                        lineHeight: "14px",
                        fontSize: "1rem",
                        fontFamily: "Sans-serif",
                        fontWeight: "800",
                        color: "#00adef",
                        fontStyle: "normal",
                        textTransform: "uppercase",
                        letterSpacing: "2px",
                      }}>
                      CoopBank Diaspora
                    </h6>
                  </Box>
                  <Box m='0px 0px 16px 0px'>
                    <h3
                      style={{
                        color: "3a3a3a",
                        fontFamily: "Poppins, sans-serif",
                        fontSize: "2rem",
                        lineHeight: "32px",
                        textAlign: "left",
                        fontWeight: "700",
                      }}>
                      Diaspora Banking
                    </h3>
                  </Box>
                  <Box>
                    <p
                      style={{
                        width: "490px",
                        fontFamily: "Open Sans, sans-serif",
                        fontSize: "1.25rem",
                        fontStyle: "normal",
                        fontWeight: "700",
                        lineHeight: "19px",
                        textSizeAdjust: "100%",
                      }}
                      class='elementor-heading-title elementor-size-default'>
                      We provide you with a distinctive array of products and
                      services tailored to your banking needs both at home and
                      abroad!
                    </p>
                  </Box>
                  <Box>
                    <p
                      style={{
                        height: "267.375px",
                        marginBottom: "16px",
                        width: "490px",
                        fontFamily: "Open Sans",
                        fontSize: "1rem",
                        fontStyle: "normal",
                        fontWeight: "400",
                        lineHeight: "29.7143px",
                        textSizeAdjust: "100%",
                        color: "#4b4f58",
                        marginBlockEnd: "16px",
                      }}>
                      CoopBank of Oromia is one of the leading private banks in
                      Ethiopia with very distinctive banking history. Diaspora
                      Banking is one of the banking segments of CoopBank which
                      has been given due emphasis. CoopBank Diaspora Account has
                      been operational since August 2012.
                      <br />
                      CoopBank Diaspora Banking is a platform designed for
                      Ethiopians who do not reside in the country to have a
                      safe, easy and convenient access to a wide range of
                      products and services carefully curated to simplify
                      cross-border banking.
                    </p>
                  </Box>
                </Box>
              </Grid>
              <Grid
                item
                lg={6}
                md={6}
                sx={{
                  // margin:"0px 80px 0px 0px",
                  alignContent: "center",
                  alignItems: "center",
                }}></Grid>
            </Grid>
          </Box>

          <Box
            id='slider-component'
            className='slider-component11'
            sx={{
              display: "flex",
              flexWrap: "wrap",
              flexDirection: "row",
              justifyContent: "center",
              paddingBottom: "77px",
              paddingTop: "85px",
              marginBottom: "-14px",
              marginTop: "-14px",
              backgroundColor: "#00adef",
              // height: "375px",
              // width: "1050px",
              position: "relative",

              opacity: "0.85",
            }}
            component={Slide}
            direction='up'
            appear={true}
            in={isSlideVisible}
            timeout={{ enter: 1000 }}
            mountOnEnter
            easing={{ fadeInUpEasing }}
            container={getContainer}>
            <Grid
              container
              rowSpacing={1}
              columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
              <Grid item>
                <StatBox
                  title={"total customers"}
                  subtitle={"total"}
                  icon={<PeopleSharpIcon />}
                  increase='57630+'
                  progress='5000+'
                />
              </Grid>
              <Grid item>
                <StatBox
                  title={"total customers"}
                  subtitle={"total"}
                  icon={<PeopleSharpIcon />}
                  increase='57630+'
                  progress='5000+'
                />
              </Grid>
              <Grid item>
                <StatBox
                  title={"total customers"}
                  subtitle={"total"}
                  icon={<PeopleSharpIcon />}
                  increase='57630+'
                  progress='5000+'
                />
              </Grid>
              <Grid item>
                <StatBox
                  title={"total customers"}
                  subtitle={"total"}
                  icon={<PeopleSharpIcon />}
                  increase='57630+'
                  progress='5000+'
                />
              </Grid>
            </Grid>
          </Box>
          <Box
            sx={{
              height: "50px",
              display: "block",
              boxSizing: "border-box",
            }}></Box>
          <Box>
            <h2
              style={{
                color: "#000000",
                margin: "0px 0px 17px 0px",
                fontFamily: "Raleway",
                fontSize: "35px",
                fontWeight: "700",
                lineHeight: "45.5px",
                textAlign: "left",
              }}>
              {" "}
              About{" "}
              <span
                style={{
                  color: "#00adef",
                  display: "inline-block",
                  fontFamily: "Playfair Display",
                  fontSize: "35px",
                  fontWeight: "700",
                  fontStyle: "italic",
                }}>
                {" "}
                Coopbank
              </span>
            </h2>
          </Box>
          <Box>
            <Box>
              <p
                style={{
                  color: "#4b4f58",
                  fontFamily: "Open Sans",
                  fontSize: "1.125rem",
                  fontWeight: "400",
                  lineHeight: "29.7143px",
                }}>
                The Coopbank is now one of the most profitable banks in Ethiopia
                having a total asset value of more than ETB 121 billion. The
                bank has 630+ branch networks, 10 million account holders, and
                more than 11,500 employees.
              </p>
              <h4
                style={{
                  color: "#3a3a3a",
                  clear: "both",
                  margin: "0px",
                  fontSize: "1.5rem",
                  fontFamily: "Poppins , sans-serif",
                  fontWeight: "700",
                }}>
                Vision
              </h4>
              <p
                style={{
                  color: "#4b4f58",
                  fontFamily: "Open Sans",
                  fontSize: "1.25rem",
                  fontWeight: "400",
                }}>
                To be the leading private bank in Ethiopia by 2025.
              </p>
              <h4
                style={{
                  color: "#3a3a3a",
                  clear: "both",
                  margin: "0px",
                  fontSize: "1.5rem",
                  fontFamily: "Poppins , sans-serif",
                  fontWeight: "700",
                }}>
                Mission
              </h4>
              <p
                style={{
                  color: "#4b4f58",
                  fontFamily: "Open Sans",
                  fontSize: "1.125rem",
                  fontWeight: "400",
                  lineHeight: "29.7143px",
                }}>
                We root our foundation in communities to provide banking
                solutions that create greater customer experience with emphasis
                to cooperatives and agro-based businesses through proper use of
                human resource and up-to-date technologies to maximize
                stakeholders’ value.
              </p>
              <h4
                style={{
                  color: "#3a3a3a",
                  clear: "both",
                  margin: "0px",
                  fontSize: "1.125rem",
                  fontFamily: "Poppins , sans-serif",
                  fontWeight: "700",
                  lineHeight: "36px",
                }}>
                Core Values
              </h4>
              <p
                style={{
                  color: "#4b4f58",
                  fontFamily: "Open Sans",
                  fontSize: "1.125rem",
                  fontWeight: "400",
                  lineHeight: "29.7143px",
                }}>
                The following set of values will serve to guide the words and
                actions of all our employees;
              </p>
              <ul>
                <li
                  style={{
                    color: "#4b4f58",
                    display: "list-item",
                    textAlign: "left",
                    fontFamily: "Open Sans",
                    fontSize: "1.125rem",
                    fontWeight: "400",
                    lineHeight: "29.7143px",
                  }}>
                  Integrity
                </li>
                <li
                  style={{
                    color: "#4b4f58",
                    display: "list-item",
                    textAlign: "left",
                    fontFamily: "Open Sans",
                    fontSize: "16px",
                    fontWeight: "400",
                    lineHeight: "29.7143px",
                  }}>
                  Customer Satisfaction
                </li>
                <li
                  style={{
                    color: "#4b4f58",
                    display: "list-item",
                    textAlign: "left",
                    fontFamily: "Open Sans",
                    fontSize: "16px",
                    fontWeight: "400",
                    lineHeight: "29.7143px",
                  }}>
                  Learning Organization
                </li>
                <li
                  style={{
                    color: "#4b4f58",
                    display: "list-item",
                    textAlign: "left",
                    fontFamily: "Open Sans",
                    fontSize: "16px",
                    fontWeight: "400",
                    lineHeight: "29.7143px",
                  }}>
                  Cost Consciousness
                </li>
                <li
                  style={{
                    color: "#4b4f58",
                    display: "list-item",
                    textAlign: "left",
                    fontFamily: "Open Sans",
                    fontSize: "16px",
                    fontWeight: "400",
                    lineHeight: "29.7143px",
                  }}>
                  Concern for Community
                </li>
              </ul>
            </Box>
          </Box>
          <Grid
            container
            spacing={2}
            sx={{
              padding: "10px",
            }}>
            <Grid
              item
              lg={6}
              md={6}
              sx={{
                padding: "15px",
                width: "100%",
              }}>
              <Box
                sx={
                  {
                    // animation: "fadeIn 1s",
                  }
                }>
                <Box
                  component={Slide}
                  direction='right'
                  in={true}
                  timeout={{ appear: 10000, enter: 600 }}>
                  <h2
                    style={{
                      color: "#000000",

                      fontFamily: "Raleway, Sans-serif",
                      fontSize: "35px",
                      fontWeight: "700",
                      lineHeight: "45.5px",
                      textAlign: "center",
                    }}>
                    {" "}
                    Diaspora{" "}
                    <span
                      style={{
                        color: "#00adef",
                        display: "inline-block",
                        fontFamily: "Playfair Display",
                        fontSize: "35px",
                        fontWeight: "700",
                        fontStyle: "italic",
                      }}>
                      {" "}
                      Accounts
                    </span>
                  </h2>
                </Box>
                <Typography component={Slide} direction='right' in={true}>
                  <p
                    style={{
                      //   margin: "0px 0px 16px",
                      margin: "0px",

                      //   paddingRight:'100px',
                      color: "#4b4f58",
                      fontFamily: "sans-serif",
                      fontSize: "16px",
                      fontWeight: "400",
                      lineHeight: "29.7143px",
                    }}>
                    Diaspora Banking Account allows Diaspora who resides and
                    works outside the country to maintain and perform domestic
                    and international transfers through their Coopbank accounts.
                  </p>
                </Typography>
                <Typography component={Slide} direction='right' in={true}>
                  <p
                    style={{
                      margin: "0px",
                      color: "#4b4f58",
                      fontFamily: "sans-serif",
                      fontSize: "16px",
                      fontWeight: "400",
                      lineHeight: "29.7143px",
                    }}>
                    » Diaspora Current Account <br />» Diaspora Fixed-Time
                    Deposit Account <br /> » Diaspora Non-Repatriable Account{" "}
                    <br /> » Ethiopian Citizen Or Origin Living In Foreign Land
                    (ECOLFL) Savings Account
                  </p>
                </Typography>
              </Box>
            </Grid>
            <Grid item lg={6} md={6} style={{ padding: "15px" }}>
              <Box>
                <Box component={Slide} direction='left' in={true}>
                  <h2
                    style={{
                      color: "#000000",

                      fontFamily: "Raleway, sans-serif",
                      fontSize: "35px",
                      fontWeight: "700",
                      lineHeight: "45.5px",
                      textAlign: "center",
                    }}>
                    {" "}
                    Diaspora{" "}
                    <span
                      style={{
                        color: "#00adef",
                        display: "inline-block",
                        fontFamily: "Playfair Display",
                        fontSize: "35px",
                        fontWeight: "700",
                        fontStyle: "italic",
                      }}>
                      {" "}
                      Loan
                    </span>
                  </h2>
                </Box>

                <Box component={Slide} direction='left' in={true}>
                  <p
                    style={{
                      color: "#4b4f58",
                      fontFamily: "sans-serif",
                      fontSize: "16px",
                      fontWeight: "400",
                      lineHeight: "29.7143px",
                      //  margin: " 16px 0px",
                    }}>
                    The bank provides business and investment loan along with
                    expertise free consultancy services on different
                    opportunities. <br />» Diaspora consumer loans
                    <br />» Mortgage loan <br />» Car/Vehicle loan <br /> »
                    Personal loan <br />» Diaspora Business loans <br /> »
                    Investment financing <br /> » Working capital loans
                  </p>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </div>
      </div>
    </Container>
  );
};

export default About;
